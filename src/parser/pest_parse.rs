use crate::parser::syntax::{
    Bind, BindRest, Block, Constrained, Expr, ExprString, FnCall, Func, Ident, Number, Pair,
    Record, Statement, Sum, Switch, Symbol,
};
use lazy_static::lazy_static;
use pest::prec_climber::{Assoc, Operator, PrecClimber};
use pest_consume::match_nodes;
use std::borrow::Cow;

pub type PestError = pest::error::Error<Rule>;

pub fn parse(input: &str) -> Result<Expr, PestError> {
    use pest_consume::Parser;
    let node = FileParser::parse(Rule::file, input)?.single()?;
    FileParser::file(node)
}

type Node<'i> = pest_consume::Node<'i, Rule, ()>;

lazy_static! {
    static ref FN_ARROW_PRECEDENCE: PrecClimber<Rule> = {
        PrecClimber::new(vec![
            Operator::new(Rule::fn_call_l_op, Assoc::Left)
                | Operator::new(Rule::fn_call_r_op, Assoc::Left),
        ])
    };
}

/// Constructs an `Expr` from a Node based on its rule type.
fn node_to_expr(input: Node) -> Result<Expr, PestError> {
    Ok(match input.as_rule() {
        Rule::constrained => Expr::Constrained(Box::new(FileParser::constrained(input)?)),
        Rule::discard => Expr::Discard,
        Rule::discard_rest => Expr::DiscardRest,
        Rule::digits => Expr::Number(FileParser::digits(input)?),
        Rule::symbol => Expr::Symbol(FileParser::symbol(input)?),
        Rule::ident => Expr::Ident(FileParser::ident(input)?),
        Rule::bind => Expr::Bind(FileParser::bind(input)?),
        Rule::bind_rest => Expr::BindRest(FileParser::bind_rest(input)?),
        Rule::pair => {
            let pair = FileParser::pair(input)?;
            Expr::Pair(Box::new(pair))
        }
        Rule::record => Expr::Rec(FileParser::record(input)?),
        Rule::sum => Expr::Sum(FileParser::sum(input)?),
        Rule::func => {
            let func = FileParser::func(input)?;
            Expr::Func(Box::new(func))
        }
        Rule::fn_call => FileParser::fn_call(input)?,
        Rule::switch => Expr::Switch(FileParser::switch(input)?),
        Rule::block => Expr::Block(FileParser::block(input)?),
        Rule::expr => FileParser::expr(input)?,
        Rule::file => FileParser::file(input)?,
        Rule::string => Expr::Str(FileParser::string(input)?),
        _ => panic!("no handler to convert rule {:?} to `Expr`", input.as_rule()),
    })
}

// Derive of `pest::Parser` also creates a public `Rule` enum in this module
#[derive(pest_derive::Parser, Clone, Copy)]
#[grammar = "parser/grammar.pest"]
struct FileParser;

#[pest_consume::parser]
impl FileParser {
    fn constrained(input: Node) -> Result<Constrained, PestError> {
        Ok(match_nodes! {
            input.into_children();
            [expr, constraint] => Constrained {
                expr: node_to_expr(expr)?,
                constraint: node_to_expr(constraint)?,
            }
        })
    }

    fn discard(input: Node) -> Result<(), PestError> {
        Ok(())
    }

    fn discard_rest(input: Node) -> Result<(), PestError> {
        Ok(())
    }

    fn digits(input: Node) -> Result<Number, PestError> {
        let digit_chars = input.as_str().chars().filter(|&c| c != '_');

        let mut result = 0u64;

        for c in digit_chars {
            let digit = c.to_digit(10).unwrap_or_else(|| panic!("Non-digit {}", c)) as u64;

            result = result
                .checked_mul(10)
                .and_then(|r| r.checked_add(digit))
                .ok_or_else(|| input.error("u64 overflow"))?;
        }

        Ok(Number(result))
    }

    fn symbol(input: Node) -> Result<Symbol, PestError> {
        // TODO: improve error message on lowercase after number (`Foo66bar`)
        Ok(Symbol(input.as_str()))
    }

    fn ident(input: Node) -> Result<Ident, PestError> {
        Ok(Ident(input.as_str()))
    }

    fn bind(input: Node) -> Result<Bind, PestError> {
        Ok(match_nodes! {
            input.into_children();
            [ident(Ident(i))] => Bind(i)
        })
    }

    fn bind_rest(input: Node) -> Result<BindRest, PestError> {
        Ok(match_nodes! {
            input.into_children();
            [ident(Ident(i))] => BindRest(i)
        })
    }

    fn string(input: Node) -> Result<ExprString, PestError> {
        Ok(match_nodes! {
            input.into_children();
            [string_node] => ExprString(Cow::Borrowed(string_node.as_str()))
        })
    }

    // `match_nodes` macro within `string` function panics if this is not defined, even though we
    // don't call this function. `string_inner` rule can't be made silent because it is atomic;
    // seems that rules cannot be both.
    fn string_inner(input: Node) -> Result<(), PestError> {
        panic!(
            "not expecting to visit string_inner parse function; should be handled by string parse"
        );
    }

    fn pair(input: Node) -> Result<Pair, PestError> {
        Ok(match_nodes! {
            input.into_children();
            [first, second] => Pair(node_to_expr(first)?, node_to_expr(second)?)
        })
    }

    fn record(input: Node) -> Result<Record, PestError> {
        Ok(match_nodes! {
            input.into_children();
            [pair(pairs)..] => Record(pairs.map(|Pair(a, b)| (a, b)).collect())
        })
    }

    fn sum(input: Node) -> Result<Sum, PestError> {
        Ok(match_nodes! {
            input.into_children();
            [variants..] => Sum(variants.map(node_to_expr).collect::<Result<Vec<Expr>, _>>()?)
        })
    }

    fn func(input: Node) -> Result<Func, PestError> {
        Ok(match_nodes! {
            input.into_children();
            [lhs, rhs] => Func { arg: node_to_expr(lhs)?, val: node_to_expr(rhs)? }
        })
    }

    fn fn_call_term(input: Node) -> Result<Expr, PestError> {
        match_nodes! {
            input.into_children();
            [expr_node] => node_to_expr(expr_node)
        }
    }

    // Must return `Expr` rather than `FnCall` in order to use precedence climber, because rhs, lhs,
    // and result must all be the same type
    #[prec_climb(fn_call_term, FN_ARROW_PRECEDENCE)]
    fn fn_call<'a>(lhs: Expr<'a>, op: Node<'a>, rhs: Expr<'a>) -> Result<Expr<'a>, PestError> {
        let (func, arg) = match op.as_rule() {
            Rule::fn_call_l_op => (lhs, rhs),
            Rule::fn_call_r_op => (rhs, lhs),
            rule => {
                panic!("Expected fn_call op token, got {:?}", rule);
            }
        };

        let fn_call = FnCall { func, arg };

        Ok(Expr::FnCall(Box::new(fn_call)))
    }

    fn switch(input: Node) -> Result<Switch, PestError> {
        Ok(match_nodes! {
            input.into_children();
            [expr_node, branch_nodes..] => {
                let branches = branch_nodes.map(node_to_expr).collect::<Result<Vec<Expr>, _>>()?;
                let expr = Box::new(node_to_expr(expr_node)?);

                Switch { expr, branches }
            }
        })
    }

    fn statement(input: Node) -> Result<Statement, PestError> {
        Ok(match_nodes! {
            input.into_children();
            [val] => Statement::ValStmt(node_to_expr(val)?),
            [lhs, rhs] => Statement::Assign { lhs: node_to_expr(lhs)?, rhs: node_to_expr(rhs)? },
        })
    }

    fn block(input: Node) -> Result<Block, PestError> {
        Ok(match_nodes! {
            input.into_children();
            [statement(statements).., expr_node] => Block {
                statements: statements.collect(),
                expr: Box::new(node_to_expr(expr_node)?)
            }
        })
    }

    fn expr(input: Node) -> Result<Expr, PestError> {
        match_nodes! {
            input.into_children();
            [expr_node] => node_to_expr(expr_node)
        }
    }

    fn file(input: Node) -> Result<Expr, PestError> {
        Ok(match_nodes! {
            input.into_children();
            [expr(e), EOI(_)] => e
        })
    }

    fn EOI(input: Node) -> Result<(), PestError> {
        Ok(())
    }
}
