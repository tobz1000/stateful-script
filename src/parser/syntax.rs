use std::borrow::Cow;
use std::fmt;

/// Any item which can appear in pattern or value locations. Produced by the parser - references and
/// types are not validated. Allows binds in value positions (to be invalidated later in the
/// compilation).
#[derive(PartialEq)]
pub enum Expr<'a> {
    Constrained(Box<Constrained<'a>>),
    Block(Block<'a>),
    Number(Number),
    Str(ExprString<'a>),
    Symbol(Symbol<'a>),
    Rec(Record<'a>),
    Sum(Sum<'a>),
    Pair(Box<Pair<'a>>),
    Func(Box<Func<'a>>),
    FnCall(Box<FnCall<'a>>),
    Switch(Switch<'a>),
    Ident(Ident<'a>),
    Bind(Bind<'a>),         // 'foo
    BindRest(BindRest<'a>), // ..'foo
    Discard,                // _
    DiscardRest,            // ..
}

/// Manual debug impl for `Expr`. Unwraps the enum and uses debug implementation of contained type
/// for more concise output.
impl<'a> fmt::Debug for Expr<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Expr::Constrained(x) => x.fmt(f),
            Expr::Block(x) => x.fmt(f),
            Expr::Number(x) => x.fmt(f),
            Expr::Str(x) => x.fmt(f),
            Expr::Symbol(x) => x.fmt(f),
            Expr::Rec(x) => x.fmt(f),
            Expr::Sum(x) => x.fmt(f),
            Expr::Pair(x) => x.fmt(f),
            Expr::Func(x) => x.fmt(f),
            Expr::FnCall(x) => x.fmt(f),
            Expr::Switch(x) => x.fmt(f),
            Expr::Ident(x) => x.fmt(f),
            Expr::Bind(x) => x.fmt(f),
            Expr::BindRest(x) => x.fmt(f),
            Expr::Discard => f.write_str("Discard"),
            Expr::DiscardRest => f.write_str("DiscardRest"),
        }
    }
}

#[derive(Debug, PartialEq)]
pub struct Constrained<'a> {
    pub expr: Expr<'a>,
    pub constraint: Expr<'a>,
}

#[derive(Debug, PartialEq)]
pub struct Block<'a> {
    pub statements: Vec<Statement<'a>>,
    pub expr: Box<Expr<'a>>,
}

#[derive(Debug, PartialEq)]
pub enum Statement<'a> {
    Assign { lhs: Expr<'a>, rhs: Expr<'a> },
    ValStmt(Expr<'a>),
}

#[derive(Debug, PartialEq)]
pub struct Number(pub u64);

#[derive(Debug, PartialEq)]
pub struct ExprString<'a>(pub Cow<'a, str>);

#[derive(Debug, PartialEq)]
pub struct Symbol<'a>(pub &'a str);

#[derive(Debug, PartialEq)]
pub struct Record<'a>(pub Vec<(Expr<'a>, Expr<'a>)>);

#[derive(Debug, PartialEq)]
pub struct Sum<'a>(pub Vec<Expr<'a>>);

#[derive(Debug, PartialEq)]
pub struct Pair<'a>(pub Expr<'a>, pub Expr<'a>);

#[derive(Debug, PartialEq)]
pub struct Func<'a> {
    pub arg: Expr<'a>,
    pub val: Expr<'a>,
}

// arg -> func
// func <- arg
#[derive(Debug, PartialEq)]
pub struct FnCall<'a> {
    pub func: Expr<'a>,
    pub arg: Expr<'a>,
}

#[derive(Debug, PartialEq)]
pub struct Switch<'a> {
    pub expr: Box<Expr<'a>>,
    pub branches: Vec<Expr<'a>>,
}

#[derive(Debug, PartialEq)]
pub struct Ident<'a>(pub &'a str);

#[derive(Debug, PartialEq)]
pub struct Bind<'a>(pub &'a str);

#[derive(Debug, PartialEq)]
pub struct BindRest<'a>(pub &'a str);
