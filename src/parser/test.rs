use crate::parser::{parse, pest_parse::PestError, syntax::Expr};
use std::fs;
use std::panic::catch_unwind;
use std::path::{Path, PathBuf};

const VALID_DATA_DIR: &str = "./src/parser/test/valid_data/";
const INVALID_DATA_DIR: &str = "./src/parser/test/invalid_data/";
const INPUT_FILE_EXT: &str = ".txt";
const RESULT_FILE_EXT: &str = ".result";

fn serialise_test_expr(expr: &Expr) -> String {
    format!("{:#?}", expr)
}

fn serialise_parse_error(err: &PestError) -> String {
    format!("{}", err)
}

#[derive(Debug)]
struct TestData {
    valid_with_results: Vec<TestWithResult>,
    valid_without_results: Vec<TestWithoutResult>,
    invalid_with_results: Vec<TestWithResult>,
    invalid_without_results: Vec<TestWithoutResult>,
}

#[derive(Debug)]
struct TestWithResult {
    input_path: PathBuf,
    input: String,
    result: String,
}

#[derive(Debug)]
struct TestWithoutResult {
    input_path: PathBuf,
    input: String,
    result_path: PathBuf,
}

/// Takes input data files, parses the contents of each, and compares to the corresponding result
/// file. Fails if any don't match.
///
/// New tests can be added by adding just the input file. Then running `cargo test` will generate
/// missing result files, instead of actually doing any tests.
#[test]
fn parse_sample_files() {
    let TestData {
        valid_with_results,
        valid_without_results,
        invalid_with_results,
        invalid_without_results,
    } = read_test_data();

    if !valid_without_results.is_empty() || !invalid_without_results.is_empty() {
        println!("New test files present without result files");

        generate_results(valid_without_results, invalid_without_results);

        panic!("New test input data present; not running tests");
    } else {
        run_tests(valid_with_results, invalid_with_results);
    }
}

fn read_test_data() -> TestData {
    fn read_dir(dir_path: &Path) -> (Vec<TestWithResult>, Vec<TestWithoutResult>) {
        let mut tests_with_results = Vec::new();
        let mut tests_without_results = Vec::new();

        // Assumes test is running from crate root
        let test_files = dir_path
            .read_dir()
            .expect("Couldn't read test data dir")
            .map(|entry| entry.unwrap())
            .filter(|entry| {
                entry
                    .file_name()
                    .to_str()
                    .map_or(false, |s| s.ends_with(INPUT_FILE_EXT))
            });

        for entry in test_files {
            if !entry.file_type().unwrap().is_file() {
                panic!("Test input data filepath {:?} is not a file", entry.path());
            }

            let input = fs::read_to_string(entry.path()).unwrap();

            let result_path = entry.path().with_file_name(format!(
                "{}{}",
                entry.file_name().to_string_lossy(),
                RESULT_FILE_EXT
            ));

            if result_path.exists() {
                if !result_path.is_file() {
                    panic!("Test result data filepath {:?} is not a file", result_path);
                }

                let expected = fs::read_to_string(result_path).unwrap();

                tests_with_results.push(TestWithResult {
                    input_path: entry.path(),
                    input,
                    result: expected,
                });
            } else {
                tests_without_results.push(TestWithoutResult {
                    input_path: entry.path(),
                    input,
                    result_path,
                });
            }
        }

        (tests_with_results, tests_without_results)
    }

    let (valid_with_results, valid_without_results) = read_dir(Path::new(VALID_DATA_DIR));
    let (invalid_with_results, invalid_without_results) = read_dir(Path::new(INVALID_DATA_DIR));

    TestData {
        valid_with_results,
        valid_without_results,
        invalid_with_results,
        invalid_without_results,
    }
}

fn generate_results(valid_tests: Vec<TestWithoutResult>, invalid_tests: Vec<TestWithoutResult>) {
    let mut successful_gens = Vec::new();
    let mut failed_gens = Vec::new();

    for test in &valid_tests {
        match catch_unwind(|| parse(&test.input)) {
            Ok(Ok(result)) => {
                let result_buf = serialise_test_expr(&result);

                match fs::write(&test.result_path, result_buf) {
                    Ok(()) => {
                        successful_gens.push(&test.result_path);
                    }
                    Err(e) => {
                        failed_gens.push((&test.input_path, e.to_string()));
                    }
                }
            }
            Ok(Err(e)) => {
                failed_gens.push((&test.input_path, format!("Failed to parse:\n{}", e)));
            }
            Err(e) => {
                failed_gens.push((
                    &test.input_path,
                    format!("Panicked while parsing:\n{:?}", e),
                ));
            }
        }
    }

    for test in &invalid_tests {
        match catch_unwind(|| parse(&test.input)) {
            Ok(Err(result)) => {
                let result_buf = serialise_parse_error(&result);

                match fs::write(&test.result_path, result_buf) {
                    Ok(()) => {
                        successful_gens.push(&test.result_path);
                    }
                    Err(e) => {
                        failed_gens.push((&test.input_path, e.to_string()));
                    }
                }
            }
            Ok(Ok(result)) => {
                failed_gens.push((
                    &test.input_path,
                    format!(
                        "Parse succeeded but should fail:\n{}",
                        serialise_test_expr(&result)
                    ),
                ));
            }
            Err(e) => {
                failed_gens.push((
                    &test.input_path,
                    format!("Panicked while parsing:\n{:?}", e),
                ));
            }
        }
    }

    if !successful_gens.is_empty() {
        println!(
            "The following result files have been written. Verify that they are correct and \
            commit or delete them:"
        );

        for gen in &successful_gens {
            println!("- {:?}", gen);
        }

        println!();
    }

    if !failed_gens.is_empty() {
        println!("Failed to generate result files for the following test input files:");

        for (path, err) in &failed_gens {
            println!("- {:?}\n{}\n", path, err);
        }
    }

    if failed_gens.is_empty() && !successful_gens.is_empty() {
        println!("Re-run `cargo test` to run tests.");
    }
}

fn run_tests(valid_tests: Vec<TestWithResult>, invalid_tests: Vec<TestWithResult>) {
    let mut fail_count = 0;

    for TestWithResult {
        input_path,
        input,
        result: expected,
    } in valid_tests
    {
        print!("{:?}: ", input_path);

        match catch_unwind(|| parse(&input)) {
            Ok(Ok(actual)) => {
                let actual_serialised = serialise_test_expr(&actual);

                if actual_serialised == expected {
                    println!("Pass");
                } else {
                    fail_count += 1;
                    println!(
                        "Failed: Parsed value does not match:\n{}\n",
                        actual_serialised
                    );
                }
            }
            Ok(Err(e)) => {
                fail_count += 1;
                println!("Failed to parse:\n{}", e);
            }
            Err(e) => {
                fail_count += 1;
                println!("{:?}", e);
            }
        }
    }

    for TestWithResult {
        input_path,
        input,
        result: expected,
    } in invalid_tests
    {
        print!("{:?}: ", input_path);

        match catch_unwind(|| parse(&input)) {
            Ok(Err(actual)) => {
                let actual_serialised = serialise_parse_error(&actual);

                if actual_serialised == expected {
                    println!("Pass");
                } else {
                    fail_count += 1;
                    println!(
                        "Failed: Generated error does not match:\n{}\n",
                        actual_serialised
                    );
                }
            }
            Ok(Ok(result)) => {
                fail_count += 1;
                println!(
                    "Parse succeeded but should fail:\n{}",
                    serialise_test_expr(&result)
                );
            }
            Err(e) => {
                fail_count += 1;
                println!("{:?}", e);
            }
        }
    }

    if fail_count > 0 {
        panic!("{} parse tests failed", fail_count);
    }
}
