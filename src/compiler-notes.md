## Compilation stages

Valid states of AST nodes, at each compilation stage, which should be expressed as Rust types.

### Parse

Source parsed to AST. All AST nodes one of the following (`Expr`):

```ts
type Expr =
  | "Constrained"
  | "Block"
  | "Number"
  | "Str"
  | "Symbol"
  | "Rec"
  | "Sum"
  | "Pair"
  | "Func"
  | "FnCall"
  | "Switch"
  | "Ident"
  | "Bind"
  | "BindRest"
  | "Discard"
  | "DiscardRest";
```

### Syntax validate

Classify & validate AST nodes by position into one of the following:

```ts
type Value0 = Exclude<Expr, "Bind" | "BindRest" | "Discard" | "DiscardRest">;
type Pattern0 = Exclude<Expr, "Func">;
type Constraint0 = Value0; // nested constraint annotations allowed until I find a good reason not to
```

### Evaluate blocks

Replace code blocks with expressions which only reference external bindings.

```ts
type Value1 = Exclude<Value0, "Block">;
type Pattern1 = Exclude<Pattern0, "Block">;
type Constraint1 = Exclude<Constraint0, "Block">;
```

### Evaluate fn calls

Replace all fn calls with their evaluations.

```ts
type Value2 = Exclude<Value1, "FnCall">;
type Pattern2 = Exclude<Pattern1, "FnCall">;
type Constraint2 = Exclude<Constraint1, "FnCall">;
```

### ...

## Block evaluation & optimisation

A brief example demonstrating both block evaluation optimisation, and calling and optimised function:

```
'f = { A 'a: int, B 'b: int, C 'c: int } => (
    // Ctx (name: constraint) or (name = assigned_val): (a: int) (b: int) (c: int)
    'd = c;  // Ctx: (a: int) (b: int) (c: int) (d = c)
    'e = d;  // Ctx: (a: int) (b: int) (c: int) (d = c) (e = d)
    // Re-evaluate `e` as `c`; Ctx: (a: int) (b: int) (c: int) (d = c) (e = c)

    add [b, e]

    // Express final value in terms of input args, eliminating intermediate bindings:
    // `add [b, c]` with Ctx: (a: int) (b: int) (c: int)
); // Ctx: (f = { A 'a: int, B 'b: int, C 'c: int } => add [b, c])

'g = { A 'aa: (4 | 5), F 'ff: int } => (
    // Ctx: (f = ...) (aa: int) (ff: int)

    f <- { A aa, B 2, C 3 }

    // To evaluate the call to `f`, revisit the optimised version of the function body with the
    // augmented context. Note that revisited functions do not close over binding from the call point,
    // only from the definition point. So `aa` and `ff` are not passed in (except via argument), and
    // more generally, a revisited function cannot have new bindings added to its context - only new
    // constraints & definite values added to argument bindings.
    // Augmented visit to `f`:
    // - Validate pattern matches ✓
    // - Now we have: Ctx: (a: (4 | 5)) (b = 2) (c = 3)
    // - Validate new constraints are subsets of original constraints, and new values are members of
    // original constraints ✓
    // - Re-evaluate previously-optimised function body: `add [b, c]` -> `add [2, 3]` -> `5`
); // Ctx: (f = ...) (g = { A 'aa: (4 | 5), F 'ff: int } => 5)
```

## Premature switch branch selection & elimination

- In a non-fully-resolved context, how to determine if a branch can never be satisfied?