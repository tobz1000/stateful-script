//! Converts a parsed AST into more specialised structures for values, patterns, and constraints,
//! and performs a bit more syntax-level validation.

pub mod value;

use crate::parser::syntax::Expr;

pub use value::Value;

// Any `Expr` is currently valid pattern syntax.
pub type Pattern<'a> = Expr<'a>;

// Valid constraint syntax is identical to value for now, because we allow nested constraint
// annotations.
type Constraint<'a> = Value<'a>;
