//! Initial evaluation.
//! - Reduces blocks to statement-less values with no intermediate bindings
//! - Removes function calls by evaluating with available context
//! - Removes switch arms where possible without type-checking.

mod constraint;
mod pattern;
mod value;

use std::collections::HashMap;
use value::Value;

type Constraint<'a> = Value<'a>;

type Bindings<'a> = HashMap<String, Binding<'a>>;

/// Tracked evaluation context
struct Ctx<'a> {
    parent_ctx: Option<&'a Ctx<'a>>,
    bindings: Bindings<'a>,
}

impl<'a> Ctx<'a> {
    fn new() -> Self {
        Ctx {
            parent_ctx: None,
            bindings: HashMap::new(),
        }
    }

    fn new_child(&'a self, new_bindings: Bindings<'a>) -> Self {
        Ctx {
            parent_ctx: Some(self),
            bindings: new_bindings,
        }
    }

    fn lookup(&self, key: &str) -> Option<Binding<'a>> {
        unimplemented!()
    }
}

/// State of a binding in a specific context after evaluation.
#[derive(Debug)]
enum Binding<'a> {
    Assigned(Value<'a>),
    Unassigned { constraint: Constraint<'a> },
}
