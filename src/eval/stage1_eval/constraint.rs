use super::Value;
use crate::parser::syntax::{ExprString, Number, Symbol};
use std::collections::HashSet;

#[derive(Debug)]
pub enum Constraint<'a> {
    // Constraint value has not yet been fully evaluated
    Unresolved {
        constraints: Vec<Value<'a>>,
    },
    // Constraint values have been fully evaluated and converted
    Resolved {
        // Set of singular, literal allowed values
        allowed_values: HashSet<ConstraintValue<'a>>,
    },
}

#[derive(Debug, PartialEq)]
pub enum ConstraintValue<'a> {
    Number(Number),
    Str(ExprString<'a>),
    Symbol(Symbol<'a>),
    Rec(Record<'a>),
    Pair(Box<Pair<'a>>),
    // TODO: func representations in constraints
}

#[derive(Debug, PartialEq)]
struct Record<'a>(pub Vec<(ConstraintValue<'a>, ConstraintValue<'a>)>);

#[derive(Debug, PartialEq)]
struct Pair<'a>(pub ConstraintValue<'a>, pub ConstraintValue<'a>);
