use super::{value, Bindings, Ctx};
use crate::eval::stage0_syntax as stage0;
use crate::parser::syntax;

#[derive(Debug, PartialEq)]
pub enum Pattern<'a> {
    Constrained(Box<value::Constrained<'a>>),
    Number(syntax::Number),
    Str(syntax::ExprString<'a>),
    Symbol(syntax::Symbol<'a>),
    Rec(Record<'a>),
    Sum(Sum<'a>),
    Pair(Box<Pair<'a>>),
    Switch(value::Switch<'a>),
    Ident(syntax::Ident<'a>),
    Bind(syntax::Bind<'a>),
    BindRest(syntax::BindRest<'a>),
    Discard,
    DiscardRest,
}

#[derive(Debug, PartialEq)]
struct Record<'a>(pub Vec<(value::Value<'a>, Pattern<'a>)>);

#[derive(Debug, PartialEq)]
struct Sum<'a>(pub Vec<Pattern<'a>>);

#[derive(Debug, PartialEq)]
struct Pair<'a>(pub Pattern<'a>, pub Pattern<'a>);

pub enum MatchResult<'a> {
    ValidMatch { new_bindings: Bindings<'a> },
    Invalid,
}

impl<'a> Pattern<'a> {
    pub fn from_stage0(from: stage0::Pattern<'a>) -> Result<Pattern<'a>, &'static str> {
        panic!()
    }

    pub fn evaluate(self, ctx: &'a Ctx<'a>) -> Result<Pattern<'a>, &'static str> {
        panic!()
    }

    pub fn get_bindings(&self) -> Bindings<'a> {
        panic!()
    }

    pub fn match_value(&self, val: value::Value<'a>, ctx: &Ctx) -> MatchResult<'a> {
        match (self, val) {
            (Pattern::Discard, _) => Ok(ctx),
            (Pattern::DiscardRest, _) => Ok(ctx),

            (Pattern::Number(x1), Value::Number(x2)) => {
                if x1 == x2 {
                    Ok(ctx)
                } else {
                    Err("number mismatch")
                }
            }
            (Pattern::Str(x1), Value::Str(x2)) => {
                if x1 == x2 {
                    Ok(ctx)
                } else {
                    Err("string mismatch")
                }
            }
            (Pattern::Symbol(x1), Value::Symbol(x2)) => {
                if x1 == x2 {
                    Ok(ctx)
                } else {
                    Err("symbol mismatch")
                }
            }
            (Pattern::Rec(x1), Value::Rec(x2)) => bind_match_rec(x1, x2, ctx),
            (Pattern::Pair(x1), Value::Pair(x2)) => bind_match_pair(x1, x2, ctx),
            (Pattern::Switch(x1), Value::Switch(x2)) => bind_match_switch(x1, x2, ctx),
            (pat, Value::Sum(x1)) => {}
            (Pattern::Bind(x1), other) => {}
            (Pattern::BindRest(x1), other) => {}
        }
    }
}
