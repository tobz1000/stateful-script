use crate::eval::stage0_syntax as stage0;
use crate::parser::syntax;

use super::pattern::{MatchResult, Pattern};
use super::{Binding, Constraint, Ctx};
use std::collections::VecDeque;

#[derive(Debug, PartialEq)]
pub enum Value<'a> {
    Constrained(Box<Constrained<'a>>),
    Number(syntax::Number),
    Str(syntax::ExprString<'a>),
    Symbol(syntax::Symbol<'a>),
    Rec(Record<'a>),
    Sum(Sum<'a>),
    Pair(Box<Pair<'a>>),
    Func(Box<Func<'a>>),
    Switch(Switch<'a>),
    Ident(syntax::Ident<'a>),
}

#[derive(Debug, PartialEq)]
pub struct Constrained<'a> {
    pub expr: Value<'a>,
    pub constraint: Constraint<'a>,
}

#[derive(Debug, PartialEq)]
struct Record<'a>(pub Vec<(Value<'a>, Value<'a>)>);

#[derive(Debug, PartialEq)]
struct Sum<'a>(pub Vec<Value<'a>>);

#[derive(Debug, PartialEq)]
struct Pair<'a>(pub Value<'a>, pub Value<'a>);

#[derive(Debug, PartialEq)]
struct Func<'a> {
    arg: Pattern<'a>,
    val: Value<'a>,
}

#[derive(Debug, PartialEq)]
pub struct Switch<'a> {
    pub expr: Box<Value<'a>>,
    pub branches: Vec<Value<'a>>,
}

impl<'a> Value<'a> {
    fn from_stage0(from: stage0::Value, ctx: Ctx<'a>) -> Result<Self, &'static str> {
        match from {
            stage0::Value::Constrained(_) => {}
            stage0::Value::Block(stage0::value::Block { statements, val }) => {
                eval_block(statements, *val, &ctx)
            }
            stage0::Value::Number(n) => Ok(Value::Number(n)),
            stage0::Value::Str(s) => Ok(Value::Str(s)),
            stage0::Value::Symbol(s) => Ok(Value::Symbol(s)),
            stage0::Value::Rec(stage0::value::Record(fields)) => {
                // TODO: duplicate keys
                let mut evaluated_fields = Vec::new();

                for (key, val) in fields {
                    let key = Value::from_stage0(key, ctx)?;
                    let val = Value::from_stage0(val, ctx)?;

                    evaluated_fields.push((key, val));
                }

                Ok(Value::Rec(Record(evaluated_fields)))
            }
            stage0::Value::Sum(stage0::value::Sum(variants)) => {
                // TODO: duplicate variants
                let variants = variants
                    .into_iter()
                    .map(|v| Value::from_stage0(v, ctx))
                    .collect::<Result<Vec<_>, _>>()?;

                Ok(Value::Sum(Sum(variants)))
            }
            stage0::Value::Pair(pair) => {
                let stage0::value::Pair(a, b) = *pair;

                let a = Value::from_stage0(a, ctx)?;
                let b = Value::from_stage0(b, ctx)?;

                Ok(Value::Pair(Box::new(Pair(a, b))))
            }
            stage0::Value::Func(func) => {
                let stage0::value::Func { arg, val } = *func;
                let arg = Pattern::from_stage0(arg, &ctx)?;
                let arg_bindings = arg.get_bindings();
                let func_ctx = ctx.new_child(arg_bindings);
                let val = Value::from_stage0(val, &func_ctx)?;

                Ok(Value::Func(Box::new(Func { arg, val })))
            }
            stage0::Value::FnCall(fn_call) => {
                let stage0::value::FnCall { func, arg } = *fn_call;

                let func = Value::from_stage0(func, &ctx)?;
                let arg_input = Value::from_stage0(arg, &ctx)?;

                match func {
                    Value::Number(_)
                    | Value::Str(_)
                    | Value::Symbol(_)
                    | Value::Rec(_)
                    | Value::Sum(_)
                    | Value::Pair(_) => Err("non-func in func position"),
                    Value::Func(func) => {
                        let Func {
                            arg: arg_pattern,
                            val: func_body,
                        } = *func;

                        match arg_pattern.match_value(arg_input, &ctx) {
                            MatchResult::ValidMatch { new_bindings } => {
                                let func_ctx = ctx.new_child(new_bindings);

                                Value::from_stage0(func_body, &func_ctx)
                            }
                            MatchResult::Invalid => Err("fn_call input did not match pattern"),
                        }
                    }
                    val @ Value::FnCall(_) | val @ Value::Switch(_) | val @ Value::Ident(_) => {
                        Ok(val)
                    }
                }
            }
            stage0::Value::Switch(stage0::value::Switch { expr, branches }) => {
                let switch_on_val = Value::from_stage0(expr, &ctx)?;
                let evaluated_branches = branches
                    .into_iter()
                    .map(|b| Value::from_stage0(b, &ctx))
                    .collect::<Result<Vec<_>, _>>()?;

                for branch in branches {
                    match branch {
                        Value::Block(_)
                        | Value::Number(_)
                        | Value::Str(_)
                        | Value::Symbol(_)
                        | Value::Rec(_)
                        | Value::Sum(_)
                        | Value::Pair(_) => {
                            return Err("non-branch in branch position");
                        }
                        Value::Func(func) => {
                            let Func {
                                arg: branch_pat,
                                val: branch_val,
                            } = *func;

                            match branch_pat.match_value(branch_val, &ctx) {
                                MatchResult::ValidMatch { new_bindings } => {
                                    let func_ctx = ctx.new_child(new_bindings);

                                    return Value::from_stage0(branch_val, func_ctx);
                                }
                                MatchResult::Invalid => {}
                            }
                        }
                        Value::FnCall(_) | Value::Switch(_) | Value::Ident(_) => {}
                    }
                }

                // TODO: determine when we can rule out branches, and error if all branches are
                // ruled out
                Ok(Value::Switch(Switch {
                    expr: Box::new(switch_on_val),
                    branches: evaluated_branches,
                }))
            }
            stage0::Value::Ident(syntax::Ident(ident)) => {
                let binding = ctx
                    .lookup(ident)
                    .ok_or("ident doesn't exist in current context")?;

                match binding {
                    Binding::Assigned(val) => Value::from_stage0(val, ctx),
                    Binding::Unassigned { .. } => Ok(val),
                }
            }
        }
    }
}

// TODO: use a heap-based stack instead of direct recursion to avoid stack overflow
fn eval_block<'a>(
    mut statements: VecDeque<stage0::value::Statement<'a>>,
    end_val: stage0::Value<'a>,
    ctx: &'a Ctx<'a>,
) -> Result<Value<'a>, &'static str> {
    match statements.pop_front() {
        Some(stage0::value::Statement::Assign { pat, val }) => match pat.match_value(val, &ctx) {
            MatchResult::ValidMatch { new_bindings } => {
                let next_ctx = ctx.new_child(new_bindings);
                Value::eval_block(statements, end_val, &next_ctx)
            }
            MatchResult::Invalid => Err("invalid assignment"),
        },
        None => Value::from_stage0(end_val, ctx),
    }
}
