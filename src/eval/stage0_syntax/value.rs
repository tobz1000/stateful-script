use crate::parser::syntax;
use crate::parser::syntax::Expr;
use std::collections::VecDeque;

use super::{Constraint, Pattern};

#[derive(Debug, PartialEq)]
pub enum Value<'a> {
    Constrained(Box<Constrained<'a>>),
    Block(Block<'a>),
    Number(syntax::Number),
    Str(syntax::ExprString<'a>),
    Symbol(syntax::Symbol<'a>),
    Rec(Record<'a>),
    Sum(Sum<'a>),
    Pair(Box<Pair<'a>>),
    Func(Box<Func<'a>>),
    FnCall(Box<FnCall<'a>>),
    Switch(Switch<'a>),
    Ident(syntax::Ident<'a>),
}

#[derive(Debug, PartialEq)]
pub struct Block<'a> {
    pub statements: VecDeque<Statement<'a>>,
    pub val: Box<Value<'a>>,
}

#[derive(Debug, PartialEq)]
pub enum Statement<'a> {
    Assign { pat: Pattern<'a>, val: Value<'a> },
    ValStmt(Value<'a>),
}

#[derive(Debug, PartialEq)]
pub struct Constrained<'a> {
    pub expr: Value<'a>,
    pub constraint: Constraint<'a>,
}

#[derive(Debug, PartialEq)]
pub struct Record<'a>(pub Vec<(Value<'a>, Value<'a>)>);

#[derive(Debug, PartialEq)]
pub struct Sum<'a>(pub Vec<Value<'a>>);

#[derive(Debug, PartialEq)]
pub struct Pair<'a>(pub Value<'a>, pub Value<'a>);

#[derive(Debug, PartialEq)]
pub struct Func<'a> {
    arg: Pattern<'a>,
    val: Value<'a>,
}

#[derive(Debug, PartialEq)]
pub struct FnCall<'a> {
    pub func: Value<'a>,
    pub arg: Value<'a>,
}

#[derive(Debug, PartialEq)]
struct Switch<'a> {
    pub expr: Box<Value<'a>>,
    pub branches: Vec<Value<'a>>,
}

impl<'a> Value<'a> {
    /// Recursive conversion from `Expr` with no evaluation. Fails on invalid syntax.
    fn from_expr(expr: Expr<'a>) -> Result<Self, &'static str> {
        match expr {
            Expr::Constrained(constrained) => {
                let syntax::Constrained { expr, constraint } = *constrained;

                let expr = Value::from_expr(expr)?;
                let constraint = Value::from_expr(constraint)?;

                Ok(Value::Constrained(Box::new(Constrained {
                    expr,
                    constraint,
                })))
            }
            Expr::Block(syntax::Block { statements, expr }) => {
                let statements = statements
                    .into_iter()
                    .map(|statement| match statement {
                        syntax::Statement::Assign { lhs, rhs } => Ok(Statement::Assign {
                            pat: lhs,
                            val: Value::from_expr(rhs)?,
                        }),
                        syntax::Statement::ValStmt(statement) => {
                            Ok(Statement::ValStmt(Value::from_expr(statement)?))
                        }
                    })
                    .collect::<Result<VecDeque<Statement>, _>>()?;

                let val = Box::new(Value::from_expr(*expr)?);

                Ok(Value::Block(Block { statements, val }))
            }
            Expr::Number(n) => Ok(Value::Number(n)),
            Expr::Str(s) => Ok(Value::Str(s)),
            Expr::Symbol(s) => Ok(Value::Symbol(s)),
            Expr::Rec(syntax::Record(fields)) => {
                let mut converted_fields = Vec::new();

                for (k_expr, v_expr) in fields {
                    let key = Value::from_expr(k_expr)?;
                    let val = Value::from_expr(v_expr)?;

                    converted_fields.push((key, val));
                }

                Ok(Value::Rec(Record(converted_fields)))
            }
            Expr::Sum(syntax::Sum(variants)) => {
                let val_variants: Result<Vec<_>, _> =
                    variants.into_iter().map(|v| Value::from_expr(v)).collect();

                Ok(Value::Sum(Sum(val_variants?)))
            }
            Expr::Pair(pair) => {
                let syntax::Pair(a, b) = *pair;
                let a_val = Value::from_expr(a)?;
                let b_val = Value::from_expr(b)?;

                Ok(Value::Pair(Box::new(Pair(a_val, b_val))))
            }
            Expr::Func(func) => {
                let syntax::Func {
                    arg: expr_arg,
                    val: expr_val,
                } = *func;
                let val_arg = expr_arg;
                let val_val = Value::from_expr(expr_val)?;

                Ok(Value::Func(Box::new(Func {
                    arg: val_arg,
                    val: val_val,
                })))
            }
            Expr::FnCall(fn_call) => {
                let syntax::FnCall { func, arg } = *fn_call;

                let func = Value::from_expr(func)?;
                let arg = Value::from_expr(arg)?;

                Ok(Value::FnCall(Box::new(FnCall { func, arg })))
            }
            Expr::Switch(syntax::Switch { expr, branches }) => {
                let expr = Value::from_expr(*expr)?;
                let branches = branches
                    .into_iter()
                    .map(Value::from_expr)
                    .collect::<Result<Vec<_>, _>>()?;

                Ok(Value::Switch(Switch {
                    expr: Box::new(expr),
                    branches,
                }))
            }
            Expr::Ident(ident) => Ok(Value::Ident(ident)),
            Expr::Bind(_) | Expr::BindRest(_) => Err("Bind not valid in value"),
            Expr::Discard | Expr::DiscardRest => Err("Discard not valid in value"),
        }
    }
}
