## LALRPOP

- Allows you to define AST construction and mapping to data structures together
    - Takes care of mapping possible sub-nodes in a type-safe way
    - Pattern matching is smart and intuitive
- Presumably produces fast, stack-based code
- Only supports LR(1) (or the more restricive LALR)
    - Currently no support for specifying which to choose of ambiguous branches, or choosing
    left-right association
        - https://github.com/lalrpop/lalrpop/issues/67
            - I would probably be using LALRPOP if this was implemented
- Error messages require knowledge of LR(1) algorithm (e.g. "could reduce or shift token")
- Requires extra build script plumbing
    - LALRPOP file errors prevent further compilation & disable rust-analyser functionality. Had to
    disable build script if I wanted to work on other parts of my program while the grammar is
    broken.
- Separates lexer from parser
    - Allows external lexer to be used, for e.g. advanced tokening logic or possible further
    performance gains

## Pest

- Iterator-based, which probably includes more heap elements. Likely less optimisable (more
unnecessary `Result`s/unwraps).
- Separates (most) AST construction from data structures entirely
    - Some may prefer this "purity", however precedence-climbing is still done in-code rather than
    .pest files
    - Does not perform any type-level logic for sub-nodes, user must do this themselves
        - Errors will only be found at run-time; grammar changes may break code
    - `pest_consume` helps with declaratively matching data structures to grammar by taking care of
    recursive looping logic, but possible sub-node types must still be taken care of by the user;
    still run-time errors.
        - This can be made to feel closer to LALRPOP-style match bindings with the
        `pest_consume::match_nodes` macro.
- Really cool sandbox editor https://pest.rs/#editor
- Allows both arbitrary choiceof ambiguous paths (by order of alternate pipe-separate choice), and
prec-climbing, with left/right precedence, for term/operator chains
- Code generation is via trait derive, integrates well with rust-analyser
    - Sneakily adds a `Rule` type to the module containing the derive
- No user-accessible lexer. Not sure to what extent lexing is separated from parsing internally.
- Some error messages unclear. Left-recursive message makes it seem as though adding a PrecClimber
will fix the compile error, when in fact a restructuring is needed first.