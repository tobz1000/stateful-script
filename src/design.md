# Design

## Primitives

- string: `"foo"`
    - must finish on the same line as it starts

- multiline string:

```
\\foo
\\bar
```

- int: `7`
- symbol (any upper-case ident): `Foo`

## ADTs

- record: `{ Foo "bar", Baz 4 }`
- sum: `Foo | Baz`

Symbols belong to the infinite sum "symbol" (`A | B | Aa | Ab | ...`).
Ints belong to the infinite sum "int" (`1 | 2 | 3 | ...`).
Strings belong to the infinite sum "string" (`"a" | "b" | "aa" | "ab" | ...`)
All types belong to the infinite sum "any".

### TODO: Record subtypes

Should records with additional fields be subtypes? e.g. is this valid:

```
'a: { Foo int, Bar int } = { Foo 6, Bar 6, Baz 5 };
```

#### If this is allowed

This could make function type requirement simpler and more universal, e.g.:

```
'f = 'a: { Foo int } => (...); // Can take any record with the field `Foo int`
```

A disadvantage here is that refactoring to bind values within a record may be unintuitive. E.g.:

```
'f = { Foo 'a } => (...); Cannot take records with additional fields
```

An implication of this being valid is that no record would ever truly be fully-resolved. E.g.
`{ Foo 6 }` would actually be a sum of all records with the field `Foo 6`, plus any other fields.
This could have tricky consequences for implementing evaluation and resolution.

#### If this is not allowed

If we decide that records with additional fields are not subtypes, we would need anothe
representation for the type constraint "any record with these specified fields, plus any other
fields". One possibility would be the pattern discard notation:

```
'f = 'a: { Foo 'a, .. } => (...);
```

The advantage of this is that refactoring binds (without changing type constraints) is more
intuitive, due to the presence of `..` in both forms:

```
'f = { Foo 'a, .. } => (...);
```

However, a major disadvantage is a logical inconsistency this introduces. Allowing `..` in a value
would imply that this exact value (i.e. set of all records with any other fields) should be matched
in a pattern. E.g.:

```
'a: { Foo int, .. }; // If this is allowed
{ Foo 'b, .. } = { Foo 5, Bar 6 }; // We would expect this to be invalid
{ Foo 'b, .. } = { Foo 5, .. }; // This would be valid instead
```

So, to use `..` in both value and pattern positions (in a useful way), we would need to introduce
an exception to pattern matching rules. If we want to avoid this exception, we would need different
syntax for the value position.

Another disadvantage is that it could lead to "colouring" of functions, into those which do and
don't allow arguments to have additional fields, similar to concrete types vs `impl Trait` in Rust.

### Accessing fields

```
'a = { Foo 4, Bar 5 };
'b = a.Foo; // Equivalent to `{ Foo 'b } = a;
```

### Record update syntax

Values are immutable, but idents can be re-bound (shadowed):

```
'a = { Foo 4, Bar 5 };
'a = { Foo 5, Bar a.Bar };
```

Shorthand for re-binding with a single field change:

```
a.Foo = 5;
```

### Tagged union shorthand

A pair of whitespace-separated values which are not in a record field position is equivalent to a
record with the fields `Variant` and `Value`:

```
Foo 5 = { Variant Foo, Value 5 };
```

This allows succinct distinction of sum variants with associated data ("tagged unions"):

```
Foo 5 | Bar int = { Variant Foo, Value 5 } | { Variant Bar, Value int };
```

## Value constraints

A constraint can be added to any value or pattern, at any level of nesting, by appending `: <value>`.

```
{ Foo 'a: int }  = { Foo 4 };
'b = { Foo 5, Bar 6 }: { Foo any, Bar (6 | 7) };
c = 7;
c: (7 | 8);
```

The value corresponding to a constraint is either the value on the left-hand side of the `:`, or for
constraints on bindinds, it's the matched value.

A value satisfies a constraint if it is a member of all allowed values expressed in the constraint.

Any value expression can be used in the constraint position. Sums behave differently in
value/pattern positions to constraint positions. Within value expressions, a sum is just another
value which can be manipulated like records & sequences. When used in a constraint, they instead
represent a multitude of allowed values.

```
'a = 1 | 2 | 3;
'b: a = 1; // `b` can be 1, 2, or 3
```

All sums within a constraint are recursively flattened when determining allowable values, such that
e.g. `a: { Foo 1 | 2 }` and `a: ({ Foo 1 } | { Foo 2 })` both represent the same allowed values of `a`.

### TODO: encapsulating sums for use as values within constraints

Currently, sums themselves cannot satisfy any expressable constraint. A way around this might be special
syntax to "encapsulate" a sum such that it is not processed when in the constraint position. E.g.:

```
'sum1 = 1 | 2 | 3;
'encapd_sum1 = {| sum1 |};
'metasum = encapd_sum1 | {| 4 | 5 |};
'a: metasum = 1; // error
'b: metasum = {| 1 | 2 | 3 |}; // okay
'c: metasum = 1 | 2 | 3; // should this (naked sum assigned to encapd sum constraint) be allowed?
```

This feels heavier than most of the other syntax so far. But the intention is generally that it
would be used for advanced type manipulation and higher-order types, so hopefully not encountered
too often in the average script.

### Equivalence & variance

Primitives (currently just ints and symbols) are trivial to compare for equality. It's also clear
how to compare records and sums of primitives, and subsequently any record/sum composed of
comparable types.

Values which would represent the same constraint, but have different structures, are not equal values.

```
'a = { Foo 1 | 2 };
'b: ({ Foo 1 } | { Foo 2 });
a = b; // Error
```

Two functions are equal if they take the same input space, and map each value to the same output
value. Functions cannot be compared programmatically in the general case.

TODO: description of function variance & function type constraints

### TODO: sum relationships syntax

With superset/subset/equal set operators, e.g. `:>`, `:<`, `:=`. `Something like:

```
(1 | 2 | 3) :> 1 | 2;
(1 | 2) :< (1 | 2 | 3);
{ Foo 1 | 2 } := ({ Foo 1 } | { Foo 2 });
```

## Patterns

Patterns are matched to values with `=`. Bindings are defined with lowercase idents. New bindings
created in patterns with `'`:

```
'foo = "bar";
'baz = foo;
```

Patterns can be arbitrarily complex:

```
{ Foo 'a, Bar 6 } = { Foo "baz", Bar 6 };
{ Foo 'a: "bar" | "baz", Qux 'b: int } = { Foo "bar", Qux 9 };
```

Bind positions can instead be discarded with `_`:

```
// Will match any value of `Bar` field as long as field is present
{ Foo 'a, Bar _ } = { Foo "baz", Bar 6 };

// Will match as long as `Bar` field is present and value is a member of `int`
{ Foo 'a, Bar _: int } = { Foo "baz", Bar 6 };
```

A value matches if:
- it is structually equal to a pattern, except for new bind locations in the pattern, after
resolving all references to previous bindings in both the pattern and value
- any new bind constraints in the pattern are satisfied by the equivalent position in the value

Order of declared fields/variables does not matter. The following is valid:

```
{ Foo "bar", Baz 5 } = { Baz 5, Foo "bar" };
Foo "bar" | Baz 5 = Baz 5 | Foo "bar";
```

Bindings cannot be made on record field tags, only field values.

Patterns must match values exactly; they cannot be a supertype. Type constraints should be used
instead.

```
{ Foo int, Bar 'b } = { Foo 6, Bar 7 }; // invalid
{ Foo _: int, Bar 'b } = { Foo 6, Bar 7 }; // valid
```

Literal sums can be used in matches, but must be an exact match; i.e. the matched value cannot just
be either-or. In other words, this is not shorthand for multiple match arms (which *is* how the `|`
is used matches in Rust).

```
'a = 1 | 2 | 3;
'b = 1;

1 | 2 | 3 = a; // valid
1 | 2 | 3 = b; // invalid

'c = 1 ?
    1 | 2 | 3 => Foo,
    1 => Bar;

c = Bar; // not `Foo`
```

### Dependent fields & tricky shadowing:

If an exactly equal value appears multiple times in a pattern, the same binding can be used in each
location:

```
'foobar = { Foo 1, Bar 1 };
{ Foo 'a, Bar 'a } = foobar; // a = 1
```

Shadowing a previously-declared binding is allowed, and the binding's previous value can also be
used in the match:

```
foobar = { Foo 1, Bar 1, Baz 4 };
'a = 4;
{ Foo 'a, Bar 'a, Baz a } = foobar; // previous `a` value of 4 is matched on; `a` is now 1
```

Dependent bindings can be transformed with functions, as long as at least one of the locations is
un-transformed:

```
{ Foo 'a, Bar ('a -> plus_one) } = { Foo 1, Bar 2 }; // a = 1;

// error: 'a should appear somewhere un-transformed
{ Foo ('a -> plus_one), Bar ('a -> plus_one) } = { Foo 2, Bar 2 }; 
```

Dependent bound values must be exactly equal in the matched-upon value.

```
foobar = { Foo 1, Bar 2 };
{ Foo 'a, Bar 'a } = foobar; // error: 'a is assigned to different values 1 and 2
```



### Binding within sums

```
Foo 'a | Bar 'b | Baz 7 = Foo 5 | Bar 6 | Baz 7;
a = 5;
b = 6;
```

### TODO: Allowing multiple matches for a single arm

Should we allow the same mapping to be performed to multiple arms which have all the same bindings?
Essentially a more concise way to do this:

```
`transform = ['a, 'b] => (...);
'expr ?
    { Foo 'a, Bar 'a } => transform [a, b],
    { Baz 'a, Qux 'a } => transform [a, b],
```

Such a shorthand would serve the same role as `|` in patterns in Rust, although we would need
different syntax, since `|` is used in sum values.

## Sequences (TODO)

```
[Foo, Bar, Baz] = { 0 Foo, 1 Bar, 2 Baz, Length 3 };
```

"Rest" pattern:

```
[..'a, 'b] = [1,2,3]; // a = [1,2]; b = 3
[.., 'c] = [1,2,3]; // c = 3
```

Specify homogenous seq type:

```
'a: [int..]  = [4,5,6];
```

Specifying sequence length with `;`:

```
'a: [..; 4] = [5,6,7,8];
[..; 'b] = [9,8,7]; // b = 3
```


## Branching

Branches are introduced with `?`:

```
'a = Foo 6 ?
    Foo 'b => b,
    Bar 'b => b + 1,
```

Order of branches matters, e.g.:
`x ? Foo _: (1 | 3) => 100, Foo _: (2 | 3) => 101` will have a different value than
`x ? Foo _: (2 | 3) => 100, Foo _: (1 | 3) => 101` for the input `Foo 3`.

## Functions

A function is a value with the syntax `<pattern> => <value>`.

A named function is created by assigning a function to a binding.

```
'foo = { Bar 'b } => { Baz b };

{ Baz 'c } = foo <- { Bar 5 };
5 = c;
```

Assign statements before return (scopes are created with `( )`):

```
'foo = { Bar 'b: int } => (
    'c = b + 1;
    { Baz 2 * c }
);
```

Functions are called with the syntaxes `<arg> -> <func>`, or `<func> <- <arg>`.

```
'foo = 'a => a + 1;
'b = 2 -> foo;
'c = foo <- 3;
```

Function call chains without parentheses are parsed with left-to-right precedence, regardless of
which way each arrow goes.

```
a -> b <- c <- d -> e = (
    'out = a -> b;
    'out = 'out <- c;
    'out = 'out <- d;
    'out = 'out -> e;
    out
);
```

Functions cannot appear in a pattern. This is because their equivalence cannot be
evaluated in the general case. Allowing partial equivalence evaluation would allow ambiguous pattern
matching, e.g. if this code was allowed:

```
'f = 'a => a;
'g = 'b => b;

'x = g ?
    f => 1,
    'other => 2;

// `x` may have a different value, depending on whether the compiler is smart enought to equate `f`
// and `g`

```

Similarly, functions cannot appear in a record key.

TODO: generics are currently modeled as functions; this would disallow them in patterns. Figure out
a way around this.

---------

### TODO: Function return type annotation

How to neatly annotate function return types? Using general constraint syntax, the return type
would appear after the function, which isn't so helpful:

```
'f = 'a: int => (
    'b = a;
    'c = b;
    "prankd"
): string
```

### Recipe for an "if"-like control flow:

```
'if = (('cond: (True | False)) { Then 'then, Else 'else }) => cond ?
    True => 'then,
    False => 'else;

'foo_cond = True;

'b = if <- foo_cond { Then 5, Else 6 };
b = 5;
```

## Generics (TODO)

```
'option = 'type => Some type | None;

'a: option <- int = Some 5;
```

### Generic function args

A "type parameter" can be introduced using dependent types within an assertion.

```
'f = { Foo 't, Bar 'x: t, Baz 'y: 't } => (...);

'v = f <- { Foo int, Bar 1, Baz 2 };
```

## TODO: Passing through contravariant parameters

Given:

```
'f = 'a => ('b: a => foo);
'g = f <- (1 | 2);
```

Is this equivalent to (a)?:

```
'g = ('b: (1 | 2)) => foo;
```

Or (b)?:

```
'g = ('b: 1 => foo) | ('b: 2 => foo);
```

The difference matters when using `g` as a constraint. e.g. if we assume (a):

```
'h: g = 1 => foo; // Error: does not satisfy constraint `(1 | 2 => foo)
```

Assuming (b):

```
'h: g = 1 => foo; // okay; satisfies `(1 => foo | 2 => foo)`
```

--------

Similarly, say we use a sum as a record key - which is effectively an input and therefore potentially
contravariant. How is this interpreted when used as a constraint?:

```
'b: { (Foo | Bar) 6 } = ...

// Is this equivalent to:
'b: { Foo 6, Bar 6 } = ...;

// or:
'b: ({ Foo 6 } | { Bar 6 }) = ...;
```
