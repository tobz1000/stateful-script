mod pest_parse;
pub mod syntax;
#[cfg(test)]
mod test;

pub use pest_parse::parse;
